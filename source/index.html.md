---
layout: markdown_page
title: Remote Only
---

## Manifesto

Remote Only work promotes:

1. Hiring and working from all over the world _instead of_ from a central location.
1. Flexible working hours _over_ set working hours.
1. Writing down and recording knowledge _over_ verbal explanations.
1. Written down processes _over_ on-the-job training.
1. Public sharing of information _over_ need-to-know access.
1. Opening every document to change by anyone _over_ top down control of documents.
1. [Asynchronous communication](https://about.gitlab.com/2015/04/08/the-remote-manifesto/) _over_ synchronous communication.
1. The results of work _over_ the hours put in.
1. Formal communication channels _over_ informal communication channels.

While there is sometimes value in the items on the right, we have come to value the items on the left more.


## Practical tips

1. People don't have to say when they are working.
1. Working long hours or weekends is not encouraged nor celebrated.
1. Use screenshots in an issue tracker instead of a whiteboard, ensuring that everyone at any time can follow the thought process.
1. Encourage non-work related communication (talking about private life on a team call).
1. Encourage group video calls for [bonding](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
1. Encourage [video calls](https://about.gitlab.com/2015/04/08/the-remote-manifesto/) between people (10 as part of onboarding).
1. Periodic summits with the whole company to get to know each other in an informal setting.
1. Encourage [teamwork and saying thanks](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)
1. Assign new hires a buddy so they have someone to reach out to.
1. Allow everyone in the company to view and edit every document.
1. Every document is always in draft, don't wait with sharing it until it is done.
1. Encourage people to write information down.

## What is it not

1. There is no main office or headquarter with multiple people _"The only way to not have people in a satellite office is not to have a main office."_
1. You do work together and communicate intensely, remote doesn't mean working independent of each other.
1. Offshoring of work, you hire around the world.
1. It is not a management paradigm, it is still a normal hierarchical organization, however there is a focus on output instead of input.
1. It is not a substitute for human interaction, people still need to collaborate, have conversations, and feel part of a team.

## How it changes the organization

1. Knowledge is written down instead of passed verbally
1. More asynchronous communication
1. Shorter and fewer meetings
1. More transparency within and outside the organization
1. Everything is public by default
1. More official communication, less informal
1. More recorded materials means less interruptions and less on-the-job training

## Advantages for employees

1. More flexibility in your daily life (kids, parents, friends, groceries, sports, deliveries)
1. No [commuting time or stress](http://www.scientificamerican.com/article/commuting-takes-its-toll/)
1. Not waste of the money for commute
1. Reduce the [interruption stress](https://www.washingtonpost.com/posteverything/wp/2014/12/30/google-got-it-wrong-the-open-office-trend-is-destroying-the-workplace/)
1. Traveling to other places without taking vacation (family, fun, etc.)
1. Free to relocate to other places
1. Some folks find it easier to communicate with rude colleguagies remotely
1. Onboarding may be less stressfull
1. Home food: better (sometimes) and cheapier
1. Cheaper taxes in some countries (for example [in Belarus you will pay only ~5%](https://dev.by/lenta/main/yurlikbez-aytishnika-vybor-sistemy-nalogooblozheniya-dlya-it-predprinimateley))

## Advantages for organizations

1. Hire great people irrespective of where they live
1. More effective employees since they have less distractions
1. More loyal employees
1. Save on office costs
1. Save on compensation due to hiring in lower cost regions
1. Selects for self starting people
1. Makes it easy to grow a company quickly
1. Encourages a focus on results, less meetings, more output
1. Cheaper taxes in some counties

## Advantages for the world

1. Reduce environmental impact due to no [commuting](http://www.scientificamerican.com/article/commuting-takes-its-toll/)
1. Reduce environmental impact due to less office space
1. Reduce inequality due to bringing better paying jobs to lower cost regions

## Disadvantages

1. Scares investors
1. Scares some partners
1. Scares some customers
1. Scares some potential employees, mostly senior non-technical hires
1. Onboarding may be harder, first month feels lonely (for some people)
1. Some people find it more difficult to work from in the same place where they sleep and watch films - dedicated workplace helps to switch the context
1. Some people find that remote working degradate communication skills
1. The need to prepare food

## Why is this possible now

1. Fast internet everywhere - 100Mb/s+ cable, 5GHz Wifi, 4G cellular
1. Video call software - Google Hangouts, Zoom
1. Mobile - Everyone has a computer in their pocket
1. Messaging apps - Slack, Mattermost
1. Issue trackers - Trello, GitHub issues, GitLab issues
1. Suggestions - GitHub Pull Requests, GitLab Merge Requests
1. Static websites - GitHub Pages, GitLab Pages
1. English proficiency - More people are learning English
1. Increasing traffic congestion in cities


At large tech companies people on the same campus now routinely do video calls instead of traveling 10 minutes each way to a different building.

More ways to enable remote-only work are continuously being developed. One example is the evolution of speech-to-text conversion software, which is [more accurate and faster than typing](http://tomtunguz.com/speech-more-accurate-faster-than-typing/), thus making written communication easier and more effective. Reading was already [faster than listening](https://en.wikipedia.org/wiki/Words_per_minute#Reading_and_comprehension), but now the process of committing speech to text + reading by recipient can be the faster way to communicate even in a 1:1 communication. For one-to-many communication, typing + reading is already faster.



## Remote only companies

1. [Buffer](https://buffer.com), see their posts about going [remote-only](https://open.buffer.com/no-office/), [the benefits](https://open.buffer.com/distributed-team-benefits/), and how they [make it work](https://open.buffer.com/buffer-distributed-team-how-we-work/).
1. [GitLab](https://about.gitlab.com), this website is hosted by GitLab, everyone is welcome to contribute to [this page](https://gitlab.com/gitlab-com/www-remoteonly-org/blob/master/source/index.html.md), also see [our handbook](https://about.gitlab.com/handbook/) for remote working practices.
1. [Groove](https://www.groovehq.com/), see their blog on being a [remote team](https://www.groovehq.com/blog/being-a-remote-team).
1. [Anybox](https://anybox.fr/), see their feedback (in french) [Télétravail généralisé, notre retour d'expérience](https://anybox.fr/blog/teletravail-generalise-confiance-et-seminaires).
1. [IOpipe](https://iopipe.com)

**Remote first companies**

- [Basecamp](https://basecamp.com/), authors of [Remote](https://37signals.com/remote)
- [Wordpress](https://wordpress.com/), authors of [The year without pants](https://www.amazon.com/Year-Without-Pants-WordPress-com-Future/dp/1118660633)

**Organizations promoting remote work**

- [Remote Year](http://www.remoteyear.com/)

**Job boards aimed at remote workers**

See a collection with reviews of [25 sites](http://skillcrush.com/2014/10/10/sites-finding-remote-work/), some of which are listed below:

1. [We Work Remotely](https://weworkremotely.com)
1. [Remote OK](https://remoteok.io)
1. [Jobspresso](https://jobspresso.co/)
1. [Working Nomads](http://www.workingnomads.co/jobs)
1. [PowerToFly](https://powertofly.com)
1. [Remote jobs on Angellist](https://angel.co/job-collections/remote)

## References

1. [After Growing to 50 People, We’re Ditching the Office Completely](https://open.buffer.com/no-office/)
1. [Remote working tips by Groove](https://www.groovehq.com/blog/remote-work-tips)
1. [Remote manifesto by GitLab](https://about.gitlab.com/2015/04/08/the-remote-manifesto/)
1. [Being tired isn’t a badge of honor](https://m.signalvnoise.com/being-tired-isn-t-a-badge-of-honor-fa6d4c8cff4e)
1. [The Day They Invented Offices](https://shift.infinite.red/a-hypothetical-conversation-with-a-real-estate-developer-in-a-world-without-offices-53cd7be0942#.pufgl7l3a)
1. [Introverts at Work: Designing Spaces for People Who Hate Open-Plan Offices](http://www.bloomberg.com/news/articles/2014-06-16/open-plan-offices-for-people-who-hate-open-plan-offices)
1. [That remote work think piece has some glaring omissions (a rant)](http://www.catehuston.com/blog/2016/04/07/that-remote-work-think-piece-has-some-glaring-omissions/)
1. [How I manage 40 people remotely](http://ryancarson.com/post/24884883426/how-i-manage-40-people-remotely)
1. [Guidelines for Effective Collaboration](https://github.com/ride/collaboration-guides)
1. [The Ultimate Guide to Remote Standups](http://blog.idonethis.com/ultimate-guide-remote-standups/)
1. [How Do You Manage Global Virtual Teams?](https://en.wikibooks.org/wiki/Managing_Groups_and_Teams/How_Do_You_Manage_Global_Virtual_Teams%3F)
1. [Getting Virtual Teams Right](https://hbr.org/2014/12/getting-virtual-teams-right)
1. [This office closed its office and remote only](http://tapes.scalevp.com/remote-only-gitlab-sytse-sid-sijbrandij/)
1. [GitLab & Buffer CEOs Talk Transparency at Scale](https://about.gitlab.com/2017/03/14/buffer-and-gitlab-ceos-talk-transparency/)
1. [GitLab’s Secret to Managing 160 Employees in 160 Locations - Interview by Y Combinator](https://blog.ycombinator.com/gitlab-distributed-startup/)
1. [GitLab's Remote Only Presentation](https://docs.google.com/presentation/d/1JHHYQvAhsudGz8QB8nqp5ScJjqyhPD3ehCoKOlZb7VE/edit#slide=id.g1d6fee80ee_0_348)
1. [Tweetstorm about the impact by Amir Salihefendic](https://twitter.com/amix3k/status/881251640795439104)
1. [Martin Fowler on remote vs. co-located](https://martinfowler.com/articles/remote-or-co-located.html)

## Contribute

It is important to understand that the whole idea of Remote Organizations is still
quite new, and it is best developed through your active participation. You can
participate in many different ways:

- You can propose or suggest any change to this website by creating a [merge request](https://gitlab.com/gitlab-com/www-remoteonly-org/merge_requests/)
- [Create an issue](https://gitlab.com/gitlab-com/www-remoteonly-org/issues/), if you have any question, or if you see some inconsistency
- Help to spread information about Remote Only organizations by sharing it on social networks
